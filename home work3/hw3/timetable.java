package hw3;

import java.util.Scanner;

public class timetable {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String exit = "Exit";

        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "Do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "Go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "Go for a walk";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "Go to the gym";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "Dance in the rain";
        scedule[5][0] = "Friday";
        scedule[5][1] = "Go shopping";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "Learn programming";


        while (true) {

            System.out.println("Please, input the day of the week:");
            String dayOfweek = scan.nextLine();

            if (dayOfweek.equals(exit)) {
                System.out.println("BYE");
                break;


            } else {


                switch (dayOfweek) {
                    case ("Sunday"):
                        System.out.println(scedule[0][1]);
                        break;
                    case ("Monday"):
                        System.out.println(scedule[1][1]);
                        break;
                    case ("Tuesday"):
                        System.out.println(scedule[2][1]);
                        break;
                    case ("Wednesday"):
                        System.out.println(scedule[3][1]);
                        break;
                    case ("Thursday"):
                        System.out.println(scedule[4][1]);
                        break;
                    case ("Friday"):
                        System.out.println(scedule[5][1]);
                        break;
                    case ("Saturday"):
                        System.out.println(scedule[6][1]);
                        break;
                    default:
                        System.out.println("Sorry, I don't understand you, please try again.");
                        break;
                }

            }
        }

    }
}
