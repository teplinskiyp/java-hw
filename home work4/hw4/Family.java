package hw4;

import java.util.Arrays;

public class Family {
    private Pets pet;
    private  Human father;
    private Human mother;
    private Human [] children;

    public Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
        this.father.setFamily(this);
        this.mother.setFamily(this);
        this.children = new Human[10];
    }
    public void addChild(Human child){
        for(int i=0;i<children.length;i++){
            if(children[i]==null){
                children[i] = child;
                break;
            }
        }
        child.setFamily(this);
    }
    public boolean deleteChild(int index){
        if(children.length<=index||children[index]==null){
            return false;
        }
        children[index].setFamily(null);
        for(int i=index;i<children.length-1;i++){

            children[i]=children[i+1];
            }

        children[children.length-1]=null;
      return true;

    }
    public int countFamily(){
        int numberChildren = 0;
        for(int i=0;i<children.length-1;i++){
            if(children[i]!=null){
                numberChildren++;
            }
        }
       return numberChildren+2;

    }

    public Pets getPet() {
        return pet;
    }

    public void setPet(Pets pet) {
        this.pet = pet;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }



    @Override
    public String toString() {
        return "Family{" +
                "pet=" + pet +
                ", father=" + father +
                ", mother=" + mother +
                ", children=" + Arrays.toString(children) +
                '}';
    }





}
