package hw4;

import java.util.Arrays;
import java.util.Objects;

public class Pets {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;


    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pets pets = (Pets) o;
        return species.equals(pets.species) && nickname.equals(pets.nickname)&&age==((Pets) o).age;
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname);
    }

    /*public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }



    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }*/



        Pets() {
            species = "Dog";
            nickname = "Rock";
            age = 5;
            trickLevel = 75;
            habits = new String[]{"eat", "drink", "sleep"};
        }

        Pets(String species, String nickname) {
            this.species = species;
            this.nickname = nickname;
        }

        Pets(String species, String nickname, int age, int trickLevel, String[] habits) {
            this.species = species;
            this.nickname = nickname;
            this.age = age;
            this.trickLevel = trickLevel;
            this.habits = habits;
        }
        void petDisplayInfo() {
           // System.out.println(species + " nickname=" + nickname + ", age= " + age + ",trickLevel=" + trickLevel + " ,habits" + Arrays.toString(habits));
            System.out.println(this);
    }
        public String toString() {
        String text = "";
        text += "Вид животного: " + species;
        text += ", nickname: " + nickname;
        text += ", age: " + age;
        text += ", trickLevel: " + trickLevel;
        text += ", habits: " + Arrays.toString(this.habits);

        return text;
    }





    void eat() {
            System.out.println("Я кушаю!");
        }

        void respond() {
            System.out.println("Привет, хозяин. Я " + nickname + " Я соскучился!");
        }

        void foul() {
            System.out.println("Нужно хорошо замести следы...");
        }

    }






