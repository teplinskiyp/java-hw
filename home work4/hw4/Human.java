package hw4;

import java.util.Arrays;

public class Human {

   private String name;
    private String surname;
    private int year;
    private int iq;
    private  String[][] schedule;
    private Pets pet;

   private Family family;

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Pets getPet() {
        return pet;
    }

    public void setPet(Pets pet) {
        this.pet = pet;
    }











    void greetPet() {
        System.out.println("Привет! " + pet.getNickname());
    }

    void describePet() {
        System.out.println("У меня есть " + pet.getSpecies() + ",ему " + pet.getAge() + "лет, он" + pet.getTrickLevel());
    }


    Human(){
        name = "Michael";
        surname = "Karleone";
        year = 1977;
        iq = 90;
        schedule = new String[][]{{"Day of Week", "Type"}};

    }

    Human (String name,String surname,int year){
        this.name=name;
        this.surname=surname;
        this.year=year;
    }



    Human(String name, String surname, int year, int iq, String schedule[][]) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule=schedule;


    }
    public void infoHuman(){
        System.out.println(this);
    }
    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(this.schedule) +
//                ", pet=" + pet +
               /* ", father=" + father +
                ", mother=" + mother +*/
                '}';
    }
}
